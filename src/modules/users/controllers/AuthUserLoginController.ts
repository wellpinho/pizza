import { Request, Response } from "express";
import { AuthUserLoginService } from "../services/AuthUserLoginService";

export class AuthUserLoginController {
  async login(req: Request, res: Response) {
    const { email, password } = req.body;

    const authUserService = new AuthUserLoginService();
    const auth = await authUserService.execute({
      email,
      password,
    });

    return res.json(auth);
  }
}
