import { Router } from "express";
import { isAuthenticated } from "../../../middlewares/isAuthenticated";
import { AuthUserLoginController } from "../controllers/AuthUserLoginController";
import { DetailUserController } from "../controllers/DetailUserController";
import { UserController } from "../controllers/UserController";

const userRoutes = Router();
const userController = new UserController();
const authUserLogin = new AuthUserLoginController();
const userDetail = new DetailUserController();

userRoutes.get("/users", userController.list);
userRoutes.post("/users", userController.create);
userRoutes.get("/user/:id", userController.show);

userRoutes.post("/login", authUserLogin.login);

userRoutes.get("/me", isAuthenticated, userDetail.handle);

export { userRoutes };
