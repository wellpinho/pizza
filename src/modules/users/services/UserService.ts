import { prismaClient } from "../../../prisma";
import bcrypt from "bcryptjs";

interface IUserRequest {
  name: string;
  email: string;
  password: string;
}

interface Id {
  id: string;
}

// recebe por params os dados do use vindos do controller
export class UserService {
  async list() {
    const users = await prismaClient.user.findMany({
      select: { id: true, name: true, email: true },
    });

    return users;
  }

  async create({ name, email, password }: IUserRequest) {
    const hashedPassword = await bcrypt.hash(password, 8);
    // verifica se não enviou email
    if (!email) {
      throw new Error("Email incorrect");
    }
    // verificar se já existe um email no banco cadastrado
    const userAlreadyExists = await prismaClient.user.findFirst({
      where: { email },
    });

    if (userAlreadyExists) {
      throw new Error("User already exists");
    }

    const user = await prismaClient.user.create({
      data: {
        name,
        email,
        password: hashedPassword,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });

    return user;
  }

  async show({ id }: Id) {
    // verificar  pois não esta funfando
    if (!id) {
      throw new Error("User id not found");
    }

    const user = await prismaClient.user.findUnique({
      where: { id },
      select: { name: true, email: true },
    });

    return user;
  }

  async update({ id }: Id, { name, email }: IUserRequest) {
    if (!id || !name || !email) {
      throw new Error("User params not found");
    }

    const user = await prismaClient.user.update({
      where: { id },
      data: { name, email },
    });

    if (!user) {
      throw new Error("User not found");
    }

    return user;
  }
}
