import { prismaClient } from "../../../prisma";
import { compare } from "bcryptjs";
import { sign } from "jsonwebtoken";

interface IAuthRequest {
  email: string;
  password: string;
}
export class AuthUserLoginService {
  async execute({ email, password }: IAuthRequest) {
    const user = await prismaClient.user.findFirst({
      where: { email },
    });

    if (!user) {
      throw new Error("User/Passowrd incorrect!");
    }

    // verifica se a senha é igual a do banco
    const passwordMatch = await compare(password, user.password);

    if (!passwordMatch) {
      throw new Error("User/Passowrd incorrect!");
    }

    // se deu tudo certo, vamos gerar o token jwt
    const token = sign(
      {
        name: user.name,
        email: user.email,
      },
      process.env.SECRET_KEY,
      {
        subject: user.id,
        expiresIn: "1d",
      }
    );

    return { id: user.id, name: user.name, email: user.email, token };
  }
}
