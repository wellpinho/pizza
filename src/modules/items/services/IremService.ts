import { prismaClient } from "../../../prisma";

interface ItemRequest {
  orderId: string;
  productId: string;
  amount: number;
}

interface ItemId {
  itemId: string;
}
export class ItemService {
  async create({ orderId, productId, amount }: ItemRequest) {
    const item = await prismaClient.item.create({
      data: { orderId, productId, amount },
    });

    return item;
  }

  async removeItem({ itemId }: ItemId) {
    const removeItem = await prismaClient.item.delete({
      where: { id: itemId },
    });

    return removeItem;
  }
}
