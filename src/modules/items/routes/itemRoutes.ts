import { Router } from "express";
import { isAuthenticated } from "../../../middlewares/isAuthenticated";
import { ItemController } from "../controllers/ItemController";

export const itemRoutes = Router();
const itemController = new ItemController();

itemRoutes.post("/order/items", isAuthenticated, itemController.create);
itemRoutes.delete("/order/items", isAuthenticated, itemController.removeItem);
