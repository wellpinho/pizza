import { Request, Response } from "express";
import { ItemService } from "../services/IremService";

export class ItemController {
  async create(req: Request, res: Response) {
    const { orderId, productId, amount } = req.body;

    const createItemService = new ItemService();

    const item = await createItemService.create({
      orderId,
      productId,
      amount,
    });

    return res.json(item);
  }

  async removeItem(req: Request, res: Response) {
    const itemId = req.query.itemId as string;

    const removeItemService = new ItemService();

    await removeItemService.removeItem({ itemId });

    return res.json({ message: "Item removed successfully" });
  }
}
