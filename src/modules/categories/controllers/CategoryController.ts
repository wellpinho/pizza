import { Request, Response } from "express";
import { CategoryService } from "../services/CategoryService";

export class CategoryController {
  async list(req: Request, res: Response) {
    const listCategories = new CategoryService();
    const categories = await listCategories.list();

    return res.json(categories);
  }
  async create(req: Request, res: Response) {
    const { name } = req.body;

    const categoryService = new CategoryService();

    const category = await categoryService.create({ name });

    return res.json(category);
  }

  async show(req: Request, res: Response) {
    const { id } = req.params;
    const showCategory = new CategoryService();
    const category = await showCategory.show({ id });

    return res.json(category);
  }
}
