import { Router } from "express";
import { isAuthenticated } from "../../../middlewares/isAuthenticated";
import { CategoryController } from "../controllers/CategoryController";

const categoryRoutes = Router();

const categoryController = new CategoryController();

categoryRoutes.get("/categories", isAuthenticated, categoryController.list);
categoryRoutes.post("/category", isAuthenticated, categoryController.create);
categoryRoutes.get("/category/:id", isAuthenticated, categoryController.show);

export { categoryRoutes };
