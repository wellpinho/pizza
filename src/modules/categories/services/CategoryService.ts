import { prismaClient } from "../../../prisma";

interface ICategoryRequest {
  name: string;
}

interface Id {
  id: string;
}

export class CategoryService {
  async list() {
    const categories = await prismaClient.category.findMany({
      select: { id: true, name: true },
    });

    return categories;
  }

  async create({ name }: ICategoryRequest) {
    if (!name) {
      throw new Error("Empty name");
    }

    const category = await prismaClient.category.create({
      data: { name: name },
      select: { id: true, name: true },
    });

    return category;
  }

  async show({ id }: Id) {
    // verificar  pois não esta funfando
    if (!id) {
      console.log(id);
      throw new Error("Category id empty");
    }

    const category = await prismaClient.category.findUnique({
      where: { id },
      select: { id: true, name: true },
    });

    if (!category) {
      throw new Error("Category not found");
    }

    return category;
  }
}
