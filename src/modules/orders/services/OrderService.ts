import { prismaClient } from "../../../prisma";

interface IOrderRequest {
  table: number;
  name: string;
}

interface OrderId {
  orderId: string;
}
export class OrderService {
  async list() {
    const orders = await prismaClient.order.findMany({
      where: { draft: false, status: false },
      orderBy: { created_at: "desc" },
    });

    return orders;
  }

  async show({ orderId }: OrderId) {
    const orders = await prismaClient.item.findMany({
      where: { id: orderId },
      include: {
        product: true,
        order: true,
      },
    });

    return orders;
  }

  async create({ table, name }: IOrderRequest) {
    const order = await prismaClient.order.create({ data: { table, name } });

    return order;
  }

  async removerTable({ orderId }: OrderId) {
    const order = await prismaClient.order.delete({
      where: { id: orderId },
    });

    return order;
  }

  async finishOrder({ orderId }: OrderId) {
    const order = await prismaClient.order.update({
      where: { id: orderId },
      data: { draft: false },
    });

    return order;
  }

  async finishOrderItem({ orderId }: OrderId) {
    const order = await prismaClient.order.update({
      where: { id: orderId },
      data: { status: true },
    });

    return order;
  }
}
