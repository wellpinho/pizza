import { Request, Response } from "express";
import { OrderService } from "../services/OrderService";

export class OrderController {
  async list(req: Request, res: Response) {
    const listOrderService = new OrderService();
    const list = await listOrderService.list();

    return res.json(list);
  }

  async show(req: Request, res: Response) {
    const orderId = req.query.orderId as string;
    const showOrderService = new OrderService();
    const orders = await showOrderService.show({ orderId });

    return res.json(orders);
  }
  async create(req: Request, res: Response) {
    const { table, name } = req.body;
    const orderService = new OrderService();
    const order = await orderService.create({ table, name });

    return res.json(order);
  }

  async removeOrder(req: Request, res: Response) {
    const orderId = req.query.orderId as string;
    const removeOrderService = new OrderService();
    const removerOrder = await removeOrderService.removerTable({ orderId });

    return res.send("Order Removed");
  }

  async finishOrder(req: Request, res: Response) {
    const orderId = req.query.orderId as string;

    const finishOrderService = new OrderService();

    await finishOrderService.finishOrder({ orderId });

    return res.json({ message: "Order Finished" });
  }

  async finishOrderItem(req: Request, res: Response) {
    const orderId = req.query.orderId as string;

    const finishOrderItemService = new OrderService();

    await finishOrderItemService.finishOrderItem({ orderId });

    return res.json({ message: "Order Item Finished" });
  }
}
