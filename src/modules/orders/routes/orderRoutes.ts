import { Router } from "express";
import { isAuthenticated } from "../../../middlewares/isAuthenticated";
import { OrderController } from "../controllers/OrderController";

export const orderRoutes = Router();
const orderController = new OrderController();

orderRoutes.get("/orders", orderController.list);
orderRoutes.get("/orders/details", orderController.show);
orderRoutes.post("/orders", isAuthenticated, orderController.create);
orderRoutes.delete("/orders", isAuthenticated, orderController.removeOrder);
orderRoutes.put("/orders/send", isAuthenticated, orderController.finishOrder);
orderRoutes.put(
  "/order/item",
  isAuthenticated,
  orderController.finishOrderItem
);
