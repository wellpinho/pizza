import { prismaClient } from "../../../prisma";

interface IProductRequest {
  name: string;
  price: string;
  description: string;
  banner: string;
  categoryId: string;
}

interface Id {
  categoryId: string;
}
export class ProductService {
  async list({ categoryId }: Id) {
    const findByCategory = await prismaClient.product.findMany({
      where: { categoryId },
    });

    return findByCategory;
  }
  async create({
    name,
    price,
    description,
    banner,
    categoryId,
  }: IProductRequest) {
    if (!name || !price || !description || !banner || !categoryId) {
      throw new Error("Params not found");
    }

    const product = await prismaClient.product.create({
      data: { name, price, description, banner, categoryId },
    });
    return product;
  }
}
