import { Router } from "express";
import multer from "multer";
import { isAuthenticated } from "../../../middlewares/isAuthenticated";
import { ProductController } from "../controllers/ProductControllers";

import uploadConfig from "./../../../config/upload";

const productRoutes = Router();
const upload = multer(uploadConfig.upload("./upload"));
const productController = new ProductController();

productRoutes.get("/categ/products", isAuthenticated, productController.list);

productRoutes.post(
  "/products",
  isAuthenticated,
  upload.single("file"),
  productController.create
);

export { productRoutes };
