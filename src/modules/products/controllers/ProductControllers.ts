import { Request, Response } from "express";
import { ProductService } from "../services/ProductService";

export class ProductController {
  async list(req: Request, res: Response) {
    const categoryId = req.query.categoryId as string;

    const listProductsByCategoryService = new ProductService();

    const products = await listProductsByCategoryService.list({
      categoryId,
    });

    return res.json(products);
  }
  async create(req: Request, res: Response) {
    const { name, price, description, categoryId } = req.body;

    const productService = new ProductService();

    if (!req.file) {
      throw new Error("error upload file");
    }

    const { originalname, filename: banner } = req.file;

    const product = await productService.create({
      name,
      price,
      description,
      banner,
      categoryId,
    });

    return res.json(product);
  }
}
