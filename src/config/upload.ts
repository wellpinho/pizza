import crypto from "crypto";
import multer from "multer";
import { extname, resolve } from "path";

export default {
  upload(folder: string) {
    return {
      storage: multer.diskStorage({
        destination: resolve(__dirname, "..", "..", folder),
        filename: (req, file, callback) => {
          const fileHash = crypto.randomBytes(16).toString("hex");
          const fileName = `${fileHash}-${file.originalname}`;

          return callback(null, fileName);
        },
      }),

      fileFilter: (req, file, cb) => {
        // Procurando o formato do arquivo em um array com formatos aceitos
        // A função vai testar se algum dos formatos aceitos do ARRAY é igual ao formato do arquivo.
        const isAccepted = ["image/png", "image/jpg", "image/jpeg"].find(
          (formatoAceito) => formatoAceito == file.mimetype
        );

        // O formato do arquivo bateu com algum aceito?
        if (isAccepted) {
          // Executamos o callback com o segundo argumento true (validação aceita)
          return cb(null, true);
        }

        // Se o arquivo não bateu com nenhum aceito, executamos o callback com o segundo valor false (validação falhouo)
        return cb(null, false);
      },
    };
  },
};
