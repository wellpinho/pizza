import { Request, Response, Router } from "express";
import { categoryRoutes } from "./modules/categories/routes/categoryRoutes";
import { itemRoutes } from "./modules/items/routes/itemRoutes";
import { orderRoutes } from "./modules/orders/routes/orderRoutes";
import { productRoutes } from "./modules/products/routes/productRoutes";
import { userRoutes } from "./modules/users/routes/userRoutes";

const routes = Router();

routes.get("/", (req: Request, res: Response) => {
  return res.json({ message: "Sujeito Pizza" });
});

routes.use(userRoutes);
routes.use(categoryRoutes);
routes.use(productRoutes);
routes.use(orderRoutes);
routes.use(itemRoutes);

export { routes };
